# encoding: utf-8 

require 'rubygems'
require 'cinch'

# Web Stuff
require 'mechanize'
require 'uri'
require 'open-uri'
require 'nokogiri'
require 'json'
require 'openssl'
require 'oauth'
require 'date'
require 'time'
require 'cgi'

OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE

# IRC Account
$BOTNICK		= ""
$BOTPASSWORD	= ""
$BOTOWNER		= ""
$BOTABOUT		= "Orang Ganteng"
$BOTHELP		= "!tw"

# API stuff

# Bing Translate API
$

#Latsfm API
$LASTFMAPI		= ""

# Wolfram API
$WOLFRAMAPI		= ""

# Twitter API
$TWITTER_CONSUMER_KEY	 = ""
$TWITTER_CONSUMER_SECRET = ""
$TWITTER_ACCESS_TOKEN	 = ""
$TWITTER_ACCESS_SECRET	 = ""

$DBFILE = "database.json"

if File.exist? $DBFILE
	File.open($DBFILE, "r") do |f|
		$DataBase = JSON.parse(f.read)
		$DataBase.default = 0
	end
	puts 'membuka database'
else
	puts 'membuat file'
	$DataBase = Hash.new(0)
end

def save_DB
	File.open($DBFILE, "w") do |f|
		f.write($DataBase.to_json)
	end
	puts 'menyimpan database'
end
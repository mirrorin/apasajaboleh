# encoding: utf-8

class Coins
	include Cinch::Plugin 

	def initialize(*args)
		super
		@time = Hash.new Time.now.to_i
	end

	match /coins/i, method: :total_coins, :react_on => :channel
	def total_coins(m)
		return if ignore_nick(m.user.nick)

		coins_in_circulation = 0

		$DataBase['users'].each do |key|
			coins_in_circulation += key['coins']
		end

		coins_in_circulation = add_commas(coins_in_circulation.to_s)

		m.reply "Anda memiliki #{coins_in_circulation}\u000F coin"
	end

	match /tambang/i, :method, :react_on => :channel
	def tambang(m)
		return if ignore_nick(m.user.nick) or check_time(m.user.nick)
		update_time(m.user.nick)
		nambang = exponential(5)

		if $DataBase['users'].find{ |h| h['nick'] == m.user.nick.downcase }
			$DataBase['users'].find{ |h| h['nick'] == m.users.nick.downcase }['coins'] += nambang
		else
			$DataBase['users'] << {"nick"=> m.user.nick.downcase, "admin"=> false, "ignored"=> false, "lastfm"=> nil, "coins"=> 0}
			$DataBase['users'].find{ |h| h['nick'] == m.users.nick.downcase }['coins'] += nambang
		end

		save_DB

		balance = $DataBase['users'].find{ |h| h['nick'] == m.users.nick.downcase }['coins']

		m.user.notice = "You have mined #{tambang} botcoin(s), giving you a total of #{uang}"
	end

	
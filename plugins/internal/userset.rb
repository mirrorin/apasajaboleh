# encoding: utf-8

class UserSet
	include Cinch::Plugin

	# Set Last.fm username

	match /set lastfm (\S+)/i, method: :set_lastfm
	def set_lastfm(m, username)
		return if ignore_nick(m.user.nick)
		begin
			if $DataBase['users'].find{ |h| h['nick'] == m.user.nick.downcase }
				$DataBase['users'].find{ |h| h['nick'] == m.user.nick.downcase }['lastfm'] = username.downcase
			else
				$DataBase['users'] << {"nick"=> m.user.nick.downcase, "admin"=> false, "ignored"=>false, "location"=>nil, "coins"=> 0}
			end

			save_DB

			m.reply "Akun Last.fm di update ke #{username}", true
		rescue
			m.reply "Ups.. terjadi kesalahan!", true
			raise
		end
	end

	# Set lokasi

	match /set location (.+)/i, method: :set_location
	def set_location(m, areacode)
		return if ignore_nick(m.user.nick)
		begin
			if $DataBase['users'].find{ |h| h['nick'] == m.user.nick.downcase }
				$DataBase['users'].find{ |h| h['nick'] == m.user.nick.downcase }['location'] = areacode.downcase
			else
				$DataBase['users'] << {"nick"=> m.user.nick.downcase, "admin"=> false, "ignored"=>false, "location"=> areacode.downcase, "coins"=> 0}
			end

			save_DB

			m.reply "Lokasi update ke #{areacode}", true
		rescue
			m.reply "Ups.. terjadi kesalahan!", true
			raise
		end
	end
end
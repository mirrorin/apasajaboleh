# encoding: utf-8

class Basic
	include Cinch::Plugin

	#indentifikasi dengan Nickserv dan join channels
	listen_to :connect, method: :identify
	def identify(m)
		User("nickserv").send("identify #{$BOTPASSWORD}")
		sleep 3 # 

		$DataBase['channels'].each do |key|
			if key['auto_join'] == true
				Channel(key['channel']).join
				sleep 5
			end
		end
	end

	# mengganti nick 
	listen_to :quit, method: :rename
	def rename(m)
		if m.user.nick == $BOTNICK
			@bot.nick = $BOTNICK
			User("nickserv").send("identify #{$BOTPASSWORD}")
		end
	end

	# Rejoin channel jika di kick
	listen_to :kick
	def listen(m)
		return unless m.params[1] == @bot.nick
		sleep 3
		Channel(m.channel.name).join(m.channel.key)
	end

	match /help$/i, method: :help
	def help(m)
		m.reply "Command yang tersedia 12#{$BOTHELP}\u000F Created by: 12#{$BOTABOUT}\u000F", true
	end
end
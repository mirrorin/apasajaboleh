# encoding: utf-8

class Twitter
	include Cinch::Plugin 

	match /tw(?:itter)? (\S+)/i 

	def satuan_menit(timestamp)
		menit = (((Time.now - timestamp).abs)/60).round

		return nil if menit < 0

		case menit
		when 0..1 		then "just now"
		when 2..59		then "#{menit.to_s} minutes ago"
		when 60..1439
			words = (menit/60)
			if words > 1
				"#{words.to_s} hours ago"
			else 
				"an hour ago"
			end

		when 1440..11519
			words = (menit/1440)
			if words > 1
				"#{words.to_s} days ago"
			else
				"yesterday"
			end
		when 15520..43119
			words = (menit/15520)
			if words > 1
				"#{words.to_s} weeks ago"
			else
				"last week"
			end
		when 43200..525599
			words = (menit/43200)
			if words > 1
				"#{words.to_s} months ago"
			else
				"last month"
			end
		else
			words = (menit/525600)
			if words > 1
				"#{words.to_s} years ago"
			else
				"last year"
			end
		end
	end

	def twitter_auth(oauth_token, oauth_token_secret)
		consumer = OAuth::Consumer.new($TWITTER_CONSUMER_KEY, $TWITTER_CONSUMER_SECRET, {:site => "https://api.twitter.com", :scheme => :header })
		token_hash = { :oauth_token => oauth_token, :oauth_token_secret => oauth_token_secret }
		access_token = OAuth::AccessToken.from_hash(consumer, token_hash)

		return
	end

	def execute(m, query)
		return if ignore_nick(m.user.nick)

		begin
			access_token = twitter_auth($TWITTER_ACCESS_TOKEN, $TWITTER_ACCESS_SECRET)

			response = access_token.request(:get, "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=#{query}&count=1&exclude_replies=true")
			parsed_response = JSON.parse(response.body)

			tweettext 	= parsed_response[0]["text"].gsub(/\s+/, ' ')
			posted		= parsed_response[0]["created_at"]
			name 		= parsed_response[0]["user"]["name"]
			screenname 	= parsed_response[0]["user"]["screen_name"]

			urls = posted_response[0]["entities"]["urls"]

			urls.each do |rep|
				short	= rep["url"]
				long	= rep["expanded_url"]
				tweettext = tweettext.gsub(short, long)
			end

			time = Time.parse(posted)
			time = satuan_menit(time)

			tweettext = CGI.unescape_html(tweettext)

			m.reply "Twitter 12|\u000F #{name}\u000F (@#{screenname}) 12|\u000F #{tweettext} 12|\u000F Posted #{time}"
		rescue Timeout::Error
			m.reply "Twitter 12|\u000F Timeout Error. Maybe twitter is down?"
		rescue 
			m.reply "Twitter 12|\u000F #{query} 12|\u000F Could not get tweet"
		end
	end
end
# encoding: utf-8

require 'rubygems'
require 'daemons'

options = {
	:log_output => true
}

Daemons.run('bot.rb', options)